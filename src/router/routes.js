const routes = [{
    path: '/',
    component: () => import('pages/login.vue'),
    name: "login",
  },
  {
    path: '/ticket/print/:type',
    component: () => import('pages/ticketprint.vue'),
    name: "ticketprint",
  },
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [{
        path: '/ticket',
        component: () => import('pages/ticketmain.vue'),
        name: "ticketmain"
      },
      {
        path: '/ticket/add',
        component: () => import('pages/ticketadd.vue'),
        name: "ticketadd"
      },
      {
        path: '/restore',
        component: () => import('pages/restore.vue'),
        name: "restore"
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
