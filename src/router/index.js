import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */


// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyCHefewE5QN4J_kZrjQfKC0IC-c_GBmdyo",
  authDomain: "ticket-3702b.firebaseapp.com",
  databaseURL: "https://ticket-3702b.firebaseio.com",
  projectId: "ticket-3702b",
  storageBucket: "ticket-3702b.appspot.com",
  messagingSenderId: "119268500028",
  appId: "1:119268500028:web:7d901b7bf89e69961c5939"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const db = firebase.firestore();

Vue.mixin({
  data() {
    return {
      appVersion: "1.0.3",
      drawer: true,

    }
  },
  methods: {
    loadingShow(message) {
      this.$q.loading.show({
        message: message
      })
    },
    loadingHide() {
      setTimeout(() => {
        this.$q.loading.hide();
      }, 500);

    },
    notifyRed(message) {
      this.$q.notify({
        position: "top",
        color: "negative",
        message: message,
        icon: "fas fa-exclamation-circle",
        timeout: 800,
      })
    },
    notifyGreen(message) {
      this.$q.notify({
        position: "top",
        color: "positive",
        message: message,
        icon: "fas fa-check",
        timeout: 800,
      })
    }
  },
})

export default function ( /* { store, ssrContext } */ ) {
  const Router = new VueRouter({
    scrollBehavior: () => ({
      x: 0,
      y: 0
    }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  return Router
}
